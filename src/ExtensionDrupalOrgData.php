<?php

namespace Drupal\extension_reference_field;

use Drupal\Core\Extension\Extension;

/**
 * Extension Drupal.org data service.
 */
class ExtensionDrupalOrgData implements ExtensionDrupalOrgDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getInfo(Extension $extension): array {
    $info = [];
    $extension_info = $extension->info;
    if (isset($extension_info['project'])) {
      $info['project'] = $extension_info['project'];
      $info['version'] = $extension_info['version'] ?? '';
      $info['datestamp'] = $extension_info['datestamp'] ?? '';
    }
    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public function getProjectUrl(Extension $extension): ?string {
    $info = $this->getInfo($extension);
    if (empty($info['project'])) {
      return NULL;
    }
    $url = 'https://www.drupal.org/project/' . $info['project'];
    return $url;
  }

}
