<?php

namespace Drupal\extension_reference_field;

use Drupal\Core\Extension\Extension;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\TypedData;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\extension_reference_field\Enum\ExtensionType;

/**
 * Class for computed extension object.
 */
class ExtensionComputed extends TypedData {

  /**
   * Computed extension.
   *
   * @var \Drupal\Core\Extension\Extension|null
   */
  protected ?Extension $extension;

  /**
   * {@inheritdoc}
   */
  public function __construct(DataDefinitionInterface $definition, $name = NULL, TypedDataInterface $parent = NULL) {
    parent::__construct($definition, $name, $parent);
    if (!$definition->getSetting('target_id')) {
      throw new \InvalidArgumentException("The definition's 'target_id' key has to specify the name of the extension property to be computed.");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    if (!is_null($this->extension)) {
      return $this->extension;
    }
    $item = $this->getParent();
    $target_id = $item->{($this->definition->getSetting('target_id'))};
    $extension_type = $item->getFieldDefinition()->getSetting('target_type');
    $extension_type_enum = ExtensionType::from($extension_type);
    $this->extension = $extension_type_enum->getExtension($target_id);
    return $this->extension;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($extension, $notify = TRUE) {
    $this->extension = $extension;
    // Notify the parent of any changes.
    if ($notify && isset($this->parent)) {
      $this->parent->onChange($this->name);
    }
  }

}
