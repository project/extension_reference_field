<?php

namespace Drupal\extension_reference_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;

/**
 * Plugin implementation of the 'extension_reference_select' widget.
 *
 * @FieldWidget(
 *   id = "extension_reference_select",
 *   label = @Translation("Extension reference select"),
 *   field_types = {
 *     "extension_reference"
 *   },
 *   multiple_values = TRUE
 * )
 */
class ExtensionReferenceSelectWidget extends OptionsSelectWidget {}
