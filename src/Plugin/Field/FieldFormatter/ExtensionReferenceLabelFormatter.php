<?php

namespace Drupal\extension_reference_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\extension_reference_field\ExtensionDrupalOrgDataInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the extension reference label formatter.
 *
 * @FieldFormatter(
 *   id = "extension_reference_label",
 *   label = @Translation("Label"),
 *   field_types = {
 *     "extension_reference"
 *   }
 * )
 */
class ExtensionReferenceLabelFormatter extends FormatterBase {

  /**
   * The extension drupal.org data service.
   *
   * @var \Drupal\extension_reference_field\ExtensionDrupalOrgDataInterface
   */
  protected ExtensionDrupalOrgDataInterface $extensionDrupalOrgData;

  /**
   * ExtensionReferenceLabelFormatter constructor.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param array $configuration
   *   The formatter configuration.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Third party settings.
   * @param \Drupal\extension_reference_field\ExtensionDrupalOrgDataInterface $extensionDrupalOrgData
   *   The extension drupal.org data service.
   */
  public function __construct($plugin_id, array $configuration, $plugin_definition, array $settings, $label, $view_mode, array $third_party_settings, ExtensionDrupalOrgDataInterface $extensionDrupalOrgData) {
    parent::__construct($plugin_id, $configuration, $plugin_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->extensionDrupalOrgData = $extensionDrupalOrgData;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('extension_reference_field.extension_drupal_org_data')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'link' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['link'] = [
      '#title' => $this->t('Link label to extension on drupal.org if available.'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('link'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->getSetting('link') ? $this->t('Link to extension on drupal.org if available.') : $this->t('No link');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $output_as_link = $this->getSetting('link');
    foreach ($items as $delta => $item) {
      $extension = $item->extension;
      $url = $output_as_link ? $this->extensionDrupalOrgData->getProjectUrl($extension) : NULL;
      if ($output_as_link && $url) {
        $elements[$delta] = [
          '#type' => 'html_tag',
          '#tag' => 'a',
          '#value' => $extension->info['name'] ?? $extension->getName(),
          '#attributes' => [
            'href' => $url,
          ],
        ];
      }
      else {
        $elements[$delta] = [
          '#plain_text' => $extension->info['name'] ?? $extension->getName(),
        ];
        $elements[$delta]['#extension'] = $extension;
      }
    }
    return $elements;
  }

}
