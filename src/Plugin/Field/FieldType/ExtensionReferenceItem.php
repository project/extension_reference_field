<?php

namespace Drupal\extension_reference_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\OptionsProviderInterface;
use Drupal\extension_reference_field\Enum\ExtensionStatus;
use Drupal\extension_reference_field\Enum\ExtensionType;

/**
 * Plugin implementation of the 'extension_reference' field type.
 *
 * @FieldType(
 *   id = "extension_reference",
 *   label = @Translation("Extension"),
 *   description = @Translation("This field stores reference to extension."),
 *   category = @Translation("Reference"),
 *   default_widget = "extension_reference_select",
 *   default_formatter = "extension_reference_label"
 * )
 */
class ExtensionReferenceItem extends FieldItemBase implements OptionsProviderInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'target_type' => [],
      'status' => ExtensionStatus::ALL->value,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = [];

    $element['target_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Extension type'),
      '#options' => ExtensionType::getOptions(),
      '#default_value' => $this->getSetting('target_type'),
      '#required' => TRUE,
      '#disabled' => $has_data,
    ];

    $element['status'] = [
      '#type' => 'select',
      '#title' => $this->t('Extension status'),
      '#options' => ExtensionStatus::getOptions(),
      '#default_value' => $this->getSetting('status'),
      '#required' => TRUE,
      '#disabled' => $has_data,
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['target_id'] = DataDefinition::create('string')
      ->setLabel(t('Extension'))
      ->setDescription(t('The ID of the extension.'))
      ->setRequired(TRUE);
    $properties['extension'] = DataDefinition::create('any')
      ->setLabel(t('Extension object'))
      ->setDescription(t('The extension object.'))
      ->setComputed(TRUE)
      ->setReadOnly(TRUE)
      ->setClass('\Drupal\extension_reference_field\ExtensionComputed')
      ->setSetting('target_id', 'target_id');
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        'target_id' => [
          'description' => 'The ID of the extension.',
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
      'indexes' => [
        'target_id' => ['target_id'],
      ],
    ];
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('target_id')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'target_id';
  }

  /**
   * Get extension object.
   */
  public function getExtension() {
    return $this->get('extension')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    // Generate random extension.
    $type_key = array_rand(ExtensionType::getAllowedExtensionTypes());
    $extensions = ExtensionType::tryFrom($type_key)->getExtensionList();
    $extension = array_rand($extensions);
    return [
      'target_id' => $extension,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleValues(AccountInterface $account = NULL) {
    return $this->getSettableValues($account);
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleOptions(AccountInterface $account = NULL) {
    return $this->getSettableOptions($account);
  }

  /**
   * {@inheritdoc}
   */
  public function getSettableValues(AccountInterface $account = NULL) {
    return array_keys($this->getSettableOptions($account));
  }

  /**
   * {@inheritdoc}
   */
  public function getSettableOptions(AccountInterface $account = NULL) {
    $target_type = $this->getSetting('target_type');
    $status = $this->getSetting('status');
    $options = [];
    $extensions = ExtensionType::tryFrom($target_type)
      ->getExtensionList($status);
    foreach ($extensions as $name => $extension) {
      $options[$name] = $extension->info['name'] ?? $name;
    }

    return $options;
  }

}
