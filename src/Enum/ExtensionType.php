<?php

/**
 * @file
 * Enum for supported extension types.
 */

namespace Drupal\extension_reference_field\Enum;

use Drupal\Core\Extension\ExtensionList;

/**
 * Extension type enum.
 */
enum ExtensionType: string {

  // Extension type cases.
  case MODULE = 'module';
  case THEME = 'theme';
  case PROFILE = 'profile';

  /**
   * Get select options.
   *
   * @return array
   *   Options.
   */
  public static function getOptions(): array {
    return [
      self::MODULE->value => t('Module'),
      self::THEME->value => t('Theme'),
      self::PROFILE->value => t('Profile'),
    ];
  }

  /**
   * Get allowed extension types.
   *
   * @return array
   *   Allowed extension types.
   */
  public static function getAllowedExtensionTypes(): array {
    return [
      self::MODULE->value,
      self::THEME->value,
      self::PROFILE->value,
    ];
  }

  /**
   * Get extension list with key as extension id and value as extension name.
   *
   * @param string $status
   *   Extension status. See \Drupal\extension_reference_field\Enum\ExtensionStatus.
   *
   * @return array
   *   Extension list.
   */
  public function getExtensionList(string $status): array {
    $extension_list = $this->getExtensionListObject()->getList();
    switch ($status) {
      case ExtensionStatus::ENABLED->value:
        return array_filter($extension_list, function ($extension) {
          return $extension->status === TRUE;
        });
      case ExtensionStatus::DISABLED->value:
        return array_filter($extension_list, function ($extension) {
          return $extension->status === FALSE;
        });
      case ExtensionStatus::ALL->value:
        return $extension_list;
      default:
        throw new \InvalidArgumentException('Invalid extension status.');
    }
  }

  /**
   * Get extension object.
   *
   * @param string $extension_id
   *   Extension ID.
   *
   * @return \Drupal\Core\Extension\Extension
   *   Extension object.
   */
  public function getExtension($extension_id) {
    return $this->getExtensionListObject()->get($extension_id);
  }

  /**
   * Get extension list object.
   *
   * @return \Drupal\Core\Extension\ExtensionList
   *   Extension list object.
   */
  protected function getExtensionListObject(): ExtensionList {
    return match ($this) {
      self::MODULE => \Drupal::service('extension.list.module'),
      self::THEME => \Drupal::service('extension.list.theme'),
      self::PROFILE => \Drupal::service('extension.list.profile'),
    };
  }

}
