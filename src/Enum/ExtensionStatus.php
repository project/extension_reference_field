<?php

/**
* @file
 * Enum for extension statuses.&#10
 *
 */

namespace Drupal\extension_reference_field\Enum;
/**
 * Extension status enum.&#10
 */
enum ExtensionStatus: string {

  // Extension status cases.
  case ENABLED = 'enabled';
  case DISABLED = 'disabled';
  case ALL = 'all';

  /**
   * Get label.
   *
   * @return string
   *   Label.
   */
  public function getLabel(): string {
    return match ($this->value) {
      self::ENABLED->value => t('Enabled(installed for theme)'),
      self::DISABLED->value => t('Disabled'),
      self::ALL->value => t('Enabled and disabled'),
    };
  }

  /**
   * Get options.
   *
   * @return array
   *   Options.
   */
  public static function getOptions(): array {
    return [
      self::ENABLED->value => self::ENABLED->getLabel(),
      self::DISABLED->value => self::DISABLED->getLabel(),
      self::ALL->value => self::ALL->getLabel(),
    ];
  }

}
