<?php

namespace Drupal\extension_reference_field;

use Drupal\Core\Extension\Extension;

/**
 * Extension Drupal.org data service interface.
 */
interface ExtensionDrupalOrgDataInterface {

  /**
   * Get extension info added by Drupal.org packaging script.
   *
   * @param \Drupal\Core\Extension\Extension $extension
   *   Extension.
   *
   * @return array
   *   Extension info.
   */
  public function getInfo(Extension $extension): array;

  /**
   * Get project URL on Drupal.org.
   *
   * @param \Drupal\Core\Extension\Extension $extension
   *   Extension.
   *
   * @return string|null
   *   Project URL.
   */
  public function getProjectUrl(Extension $extension): ?string;

}
