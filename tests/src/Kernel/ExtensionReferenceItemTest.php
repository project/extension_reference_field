<?php

namespace Drupal\Tests\extension_reference_field\Kernel;

use Drupal\Core\Extension\Extension;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\Tests\extension_reference_field\Traits\CreateExtensionReferenceFieldTestTrait;
use Drupal\Tests\field\Kernel\FieldKernelTestBase;

/**
 * Tests for extension_reference field type.
 *
 * @group extension_reference_field
 */
class ExtensionReferenceItemTest extends FieldKernelTestBase {

  use CreateExtensionReferenceFieldTestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'extension_reference_field',
    'text',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->createExtensionReferenceField(
      'entity_test',
      'entity_test',
      'field_extension_reference',
      'Extension reference',
      'module',
    );
  }

  /**
   * Tests processed properties.
   */
  public function testExtensionReferenceItem() {
    // Create an entity.
    $entity = EntityTest::create();
    $entity->field_extension_reference->target_id = 'text';
    $entity->name->value = $this->randomMachineName();
    $entity->save();

    // Test field item.
    $entity = EntityTest::load($entity->id());
    $this->assertInstanceOf(FieldItemListInterface::class, $entity->field_extension_reference, 'Field implements interface.');
    $this->assertInstanceOf(FieldItemInterface::class, $entity->field_extension_reference[0], 'Field item implements interface.');
    $this->assertEquals('text', $entity->field_extension_reference->target_id);

    // Test computed extension property.
    $this->assertInstanceOf(Extension::class, $entity->field_extension_reference->extension, 'Extension reference is an instance of Extension class.');
    $this->assertEquals('text', $entity->field_extension_reference->extension->getName());
  }

}
