<?php

namespace Drupal\Tests\extension_reference_field\Kernel;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\Tests\extension_reference_field\Traits\CreateExtensionReferenceFieldTestTrait;

/**
 * Tests using entity fields of the extension_reference_field field type.
 *
 * @group extension_reference_field
 */
class ExtensionReferenceFieldFormatterTest extends EntityKernelTestBase {

  use CreateExtensionReferenceFieldTestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'extension_reference_field',
    'media',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->createExtensionReferenceField(
      'entity_test',
      'entity_test',
      'field_extension_reference',
      'Extension reference',
      'module',
    );
  }

  /**
   * Tests the default formatter.
   */
  public function testDefaultFormatter() {
    $entity = EntityTest::create([
      'name' => 'Test entity',
      'user_id' => 1,
      'field_extension_reference' => 'media',
    ]);
    $entity->save();

    // Test a render array is correct.
    $view = $entity->get('field_extension_reference')->view([
      'type' => 'extension_reference_label',
      'settings' => [
        'link' => FALSE,
      ],
    ]);
    $this->assertEquals('Media', $view[0]['#plain_text']);
  }

}
