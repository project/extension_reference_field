<?php

namespace Drupal\Tests\extension_reference_field\Functional;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Language\LanguageInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\Tests\extension_reference_field\Traits\CreateExtensionReferenceFieldTestTrait;
use Drupal\Tests\field\Functional\FieldTestBase;

/**
 * Tests the extension_reference field widget.
 *
 * @group extension_reference_field
 */
class ExtensionReferenceFieldWidgetTest extends FieldTestBase {

  use CreateExtensionReferenceFieldTestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'extension_reference_field',
    'entity_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A field storage with cardinality 1 to use in this test class.
   *
   * @var \Drupal\field\Entity\FieldStorageConfig
   */
  protected $field;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->createExtensionReferenceField(
      'entity_test',
      'entity_test',
      'field_extension_reference',
      'Extension reference',
      'module',
    );
    $this->field = FieldConfig::loadByName('entity_test', 'entity_test', 'field_extension_reference');
    $this->drupalLogin($this->drupalCreateUser(
      [
        'view test entity',
        'administer entity_test content',
      ]));
  }

  /**
   * Tests the default widget.
   */
  public function testDefaultWidget() {
    EntityFormDisplay::load('entity_test.entity_test.default')
      ->setComponent('field_extension_reference', [
        'type' => 'extension_reference_select',
      ])
      ->save();

    $entity = EntityTest::create([
      'name' => 'Test entity',
      'user_id' => 1,
    ]);
    $entity->save();

    // Test a exist field widget.
    $this->drupalGet('entity_test/manage/' . $entity->id() . '/edit');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->fieldExists('field_extension_reference');

    // Test a valid field value.
    $edit = [
      'field_extension_reference' => 'entity_test',
    ];
    $this->submitForm($edit, 'Save');
    $this->assertFieldValues($entity, 'field_extension_reference', ['entity_test'], LanguageInterface::LANGCODE_DEFAULT, 'target_id');
  }

}
