<?php

namespace Drupal\Tests\extension_reference_field\Traits;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Provides a trait to create an extension_reference field.
 */
trait CreateExtensionReferenceFieldTestTrait {

  /**
   * Creates an extension_reference field.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   * @param string $field_name
   *   The field name.
   * @param string $field_label
   *   The field label.
   * @param string $target_entity_type
   *   The target entity type.
   * @param int $cardinality
   *   The cardinality.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createExtensionReferenceField($entity_type, $bundle, $field_name, $field_label, $target_entity_type, $cardinality = 1) {
    if (!FieldStorageConfig::loadByName($entity_type, $field_name)) {
      FieldStorageConfig::create([
        'field_name' => $field_name,
        'type' => 'extension_reference',
        'cardinality' => $cardinality,
        'entity_type' => $entity_type,
        'settings' => [
          'target_type' => $target_entity_type,
          'status' => 'all',
        ],
      ])->save();
    }
    if (!FieldConfig::loadByName($entity_type, $bundle, $field_name)) {
      FieldConfig::create([
        'field_name' => $field_name,
        'entity_type' => $entity_type,
        'bundle' => $bundle,
        'label' => $field_label,
      ])->save();
    }
  }

}
