# Extension Reference Field Module

## Description

This Drupal module provides an extension reference field type. It allows you to create fields that reference other extensions in your Drupal site.
All extension types are supported, including modules, themes, and profiles.

## Installation
Install as you would normally install a contributed Drupal module. Visit https://www.drupal.org/docs/extending-drupal/installing-modules for further information.

## Configuration

After installing the module, you can create an extension reference field by following these steps:

1. Navigate to the Manage Fields page of the content type you want to add the field to (`/admin/structure/types/manage/[content-type]/fields`).
2. Click the Add field button.
3. Select Extension from the list of field types.
4. Select the extension type you want to reference from the Extension type dropdown.
5. Save the field settings.

## Maintainers

- Pablo Pukha (Pablo_Pukha) - https://www.drupal.org/u/Pablo_Pukha
